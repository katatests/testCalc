package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {

	rones := make(map[string]int64)
	rones["I"] = int64(1)
	rones["II"] = int64(2)
	rones["III"] = int64(3)
	rones["IV"] = int64(4)
	rones["V"] = int64(5)
	rones["VI"] = int64(6)
	rones["VII"] = int64(7)
	rones["VIII"] = int64(8)
	rones["IX"] = int64(9)
	rones["X"] = int64(10)

	ronesrev := make(map[int64]string)
	ronesrev[1] = "I"
	ronesrev[2] = "II"
	ronesrev[3] = "III"
	ronesrev[4] = "IV"
	ronesrev[5] = "V"
	ronesrev[6] = "VI"
	ronesrev[7] = "VII"
	ronesrev[8] = "VIII"
	ronesrev[9] = "IX"

	rdecrev := make(map[int64]string)
	rdecrev[10] = "X"
	rdecrev[20] = "XX"
	rdecrev[30] = "XXX"
	rdecrev[40] = "XL"
	rdecrev[50] = "L"
	rdecrev[60] = "LX"
	rdecrev[70] = "LXX"
	rdecrev[80] = "LXXX"
	rdecrev[90] = "XC"
	rdecrev[100] = "C"

	//Сканер для чтения StdIn
	sc := bufio.NewScanner(os.Stdin)
	//Чтение строки из StdIn
	for sc.Scan() {
		//Обрезка пробелов
		txt := strings.TrimSpace(sc.Text())
		//Команда для выхода
		if txt == "exit" {
			break
		}

		isArabic := false
		isRoman := false
		args := [2]int64{0, 0}
		op := ""
		skip := false

		//Разбить прочитанную строку по пробелам
		calcparts := strings.Split(txt, " ")

		//Результат разбиения должен содержать 3 элемента
		if len(calcparts) != 3 {
			fmt.Println("Ошибка фрмата ввода. Вражение должно иметь вид: a1 op a2")
		} else {
			//Цикл по содержимому строки
			for i := 0; i < 3; i++ {
				if i == 1 { //Элемент с индексом 1 всегда оператор
					op = calcparts[i]
				} else {
					//Преобрпзование текстового представления аргументов в int64
					val, err := strconv.ParseInt(calcparts[i], 10, 64)
					val1, ok := rones[calcparts[i]]
					//Удачное преобразование арабских цифр
					if err == nil && val >= 1 && val <= 10 {
						isArabic = true
						//Удачное преобразование римских цифр
					} else if ok {
						isRoman = true
						//Ошибка преобразования
					} else {
						fmt.Println("Ошибка формата чисел. Допускается ввод арабских 1,2,3,4,5,6,7,8,9,10 либо римских I,II,III,IV,V,IV,VI,VII,VIII,IX,X")
						skip = true
					}

					//
					switch {
					case isArabic && isRoman:
						fmt.Println("Ошибка. Смешивать арабские и римские цифры в одном выражении запрещено.")
						skip = true
					case isArabic:
						if i == 0 {
							args[0] = val
						}
						if i == 2 {
							args[1] = val
						}
					case isRoman:
						if i == 0 {
							args[0] = val1
						}
						if i == 2 {
							args[1] = val1
						}
					}
				}

				if skip {
					break
				}
			}

			//Вычисление результата
			res := int64(0)
			if !skip {
				switch op {
				case "+":
					res = args[0] + args[1]
				case "-":
					res = args[0] - args[1]
				case "*":
					res = args[0] * args[1]
				case "/":
					res = args[0] / args[1]
				default:
					fmt.Println("Ошибка. Неизвестная математическая операция. Поддерживаются только: +,-,*,/.")
					skip = true
				}
			}

			//Преобразование результата в строку
			if !skip {
				if isArabic {
					fmt.Println(res)
				} else {
					if res <= 0 {
						fmt.Println("Ошибка результата. Для римских цифр не возможно вывести результат меньше или равный 0.")
					} else {
						ones := res % 10
						decs := (res / 10) * 10

						ones_str, ok := ronesrev[ones]
						decs_str, ok1 := rdecrev[decs]

						if ok && ok1 {
							fmt.Printf("%s%s\n", decs_str, ones_str)
						} else if ok {
							fmt.Printf("%s\n", ones_str)
						} else if ok1 {
							fmt.Printf("%s\n", decs_str)
						} else {
							fmt.Println("Ошибка результата. Невозможно вывести результат в римских цифрах.")
						}
					}
				}
			}
		}
	}
}
